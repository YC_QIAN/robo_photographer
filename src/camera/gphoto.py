# from https://sourceforge.net/projects/gphoto/files/ download libgphoto
# usb error : ps -A | grep gphoto2, kill -9 all processes except itself

import gphoto2cffi as gp


def take_real_picture():
    # List all attached cameras that are supported
    cams = gp.list_cameras()

    # Get an instance for the first supported camera
    my_cam = gp.Camera()

    # Capture an image to the camera's RAM and get its data
    imgdata = my_cam.capture(True)

