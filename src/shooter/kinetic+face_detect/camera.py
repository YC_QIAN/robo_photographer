import mxnet as mx
from mtcnn_detector import MtcnnDetector
#import sys
#sys.path.remove('/opt/ros/kinetic/lib/python2.7/dist-packages')
import cv2
import numpy as np
from pylibfreenect2 import Freenect2, SyncMultiFrameListener
from pylibfreenect2 import FrameType, Registration, Frame, FrameMap
from pylibfreenect2 import createConsoleLogger, setGlobalLogger
from pylibfreenect2 import LoggerLevel

detector = MtcnnDetector(model_folder='model', ctx=mx.cpu(0), num_worker = 4 , accurate_landmark = False)

fn=Freenect2()
if fn.enumerateDevices()>0:
	device=fn.openDefaultDevice()
	listener=SyncMultiFrameListener()
	device.setColorFrameListener(listener)
	device.setIrAndDepthFrameListener(listener)
	device.start()

	while True:
		frames=listener.waitForNewFrame()
		rgb=frames['color']
		#ir=frames['ir']
		#depth=frames['depth']
		
		img=rgb.asarray()
		img=cv2.cvtColor(img,cv2.COLOR_BGRA2BGR)
		
		results=detector.detect_face(img)
		if results is not None:
			faces=results[0]
			points=results[1]
			for b in faces:
				cv2.rectangle(img, (int(b[0]), int(b[1])), (int(b[2]), int(b[3])), (255, 255, 255))

			#for p in points:
				#for i in range(5):
					#cv2.circle(img, (p[i], p[i + 5]), 1, (0, 0, 255), 2)
		
		cv2.imshow('picture',img)
		key=cv2.waitKey(1)
		listener.release(frames)
		if key==ord('q'):
			break

	device.stop()
	device.close()

