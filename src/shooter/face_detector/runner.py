#coding=utf-8

import base64
from typing import Dict

import cv2
from aip import AipFace

APP_ID='15073865'
API_KEY='9MgxDdPf6SGKLKsqLY42ZtqO'
SECRET_KEY='iUXf1cMCzLHSiCmD8fHL3hTC4SZdDr2G'

# 以下是参照物测算结果
F=500
L=5

# return the face list:[[x,y,width,height],...]
def face_detect(img,type='BASE64'):
	client=AipFace(APP_ID,API_KEY,SECRET_KEY)
	options={}
	options['face_field']='beauty'

	res=client.detect(img,type,options)  # type: Dict
	faces=[]
    for item in res['result']['face_list']:
        loc=item['location']
        x,y=int(loc['left']),int(loc['top'])
        w,h=int(loc['width']),int(loc['height'])
        faces.append([x,y,w,h])

	return faces

def process_img(img_path,detect_res):
	img=cv2.imread(img_path)
	faces=detect_res['result']['face_list']
	for face in faces:
		loc=face['location']
		x,y=int(loc['left']), int(loc['top'])
		width,height=int(loc['width']),int(loc['height'])
		cv2.rectangle(img,(x,y),(x+width,y+height),(0,255,0),5)
		img=cv2.resize(img,(640,480))
		cv2.imshow('face',img)
		cv2.waitKey(-1)

def cvt2base64(path):
	with open("../../../img_save.png",'rb') as f:
		str=base64.b64encode(f.read())
		return str

def data2base64(data):
	str = base64.b64encode(data)
	return str


def distance(l):
	return L*F/l

def get_motions(detect_res):
	# print detect_res
	if detect_res['result']:
		faces=detect_res['result']['face_list']
	else:
		faces=[]
	motions=[]
	for face in faces:
		motion={}
		width=face['location']['width']
		height=face['location']['height']
		yaw=face['angle']['yaw']
		motion['radius']=(distance(width)+distance(height))/2
		# yaw的正负与方向的关系在实验室的纸上
		motion['theta']=yaw
		motions.append(motion)
	return motions

if __name__=='__main__':

	# 调用方式：motions=get_motions(face_detect(cvt2base64(img_url)))
	# motions=[
	#   {
	#       radius=XXX, //半径,cm
	# 		theta=XXX   //圆心角,°,为正左转
	#   },{},...
	# ]
	str=cvt2base64('../../../andi.jpg')
	res=face_detect(str)
	process_img('../../../andi.jpg',res)

	print(res)
