import cv2

'''
此脚本适用于计算参照物大小

5cm * 5cm 的矩形在50cm的距离上尺寸为50px * 50px 
'''

if __name__=='__main__':
	img=cv2.imread('image.png')
	img=cv2.resize(img,(640,480))
	img=img[180:280,300:380]
	# cv2.imshow('img',img)

	grey=cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
	cv2.imshow('grey',grey)
	val,thresh=cv2.threshold(grey,64,255,cv2.THRESH_BINARY_INV)
	cv2.imshow('thresh',thresh)

	image,contours,hierarchy=cv2.findContours(thresh,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
	for cnt in contours:
		e=0.03*cv2.arcLength(cnt,True)
		approx=cv2.approxPolyDP(cnt,e,True)
		print(approx)

	cv2.waitKey(-1)