#!/usr/bin/env python
# currently the node will read the image every @SEND_PERIOD second
# get radius and theta obtained from face_detect
# go to the point facing the person
import rospy
from numpy.core.multiarray import ndarray
from sensor_msgs.msg import CompressedImage
import numpy as np
import math
import datetime

from face_detector import runner as r
from robo_photographer.srv import MoveRobot

image_np = np.array([])  # type: ndarray
SEND_PERIOD = 0.5

# moveRobot = m.MoveRobot()
is_moving = False
MOVING_PERIOD = 120
moved_period = 0


def move_robot_client(x, y, theta):
    rospy.wait_for_service('move_robot')
    try:
        move_robot = rospy.ServiceProxy('move_robot', MoveRobot)
        print "Requesting"
        res = move_robot(x, y, theta)
    except rospy.ServiceException, e:
        print "Service call failed : %s"%e


def send_image(event):
    if image_np.size == 0:
        return
    str = r.data2base64(image_np)
    print r.face_detect(str)
    motions = r.get_motions(r.face_detect(str))
    if len(motions) == 0:
        dis = 1
        theta = 1
        print "cir"
        x = dis*math.sin(theta)
        y = dis*math.cos(theta)
        print x, y, -theta
        move_robot_client(x, y, -theta)
        return
    # calculate the avg radius and theta of all faces
    sum_radius = 0
    sum_theta = 0
    for motion in motions:
        sum_radius += motion['radius']
        sum_theta += motion['theta']
    avg_radius = sum_radius/len(motions)
    avg_theta = sum_theta/len(motions)
    radius = avg_radius/100.0  # m
    left_first = (avg_theta > 0)
    theta = math.radians(90-abs(avg_theta))  # rad

    dis = radius*math.cos(theta)
    print dis
    print theta
    # TODO: meet obstacle, stop, say something and wait

    global moved_period
    if moved_period > MOVING_PERIOD:
        moved_period = 0
        return

    global is_moving
    if not is_moving:
        is_moving = True
        start_time = datetime.datetime.now()
        move_robot_client(dis * math.sin(theta), dis * math.cos(theta), -theta)
        end_time = datetime.datetime.now()
        moved_period += (end_time-start_time).seconds

    is_moving = False


def callback(data):
    np_arr = np.fromstring(data.data, np.uint8)  # type: ndarray
    global image_np
    image_np = np_arr
    # image_np = cv2.imdecode(np_arr, cv2.IMREAD_COLOR)

    # print type(image_np)
    # cv2.imshow('cv_img', image_np)
    # cv2.waitKey(2)


def cam_listener():
    rospy.init_node('cam_listener', anonymous=True)
    rospy.Subscriber('/raspicam_node/image/compressed', CompressedImage, callback, queue_size=10)
    rospy.Timer(rospy.Duration(SEND_PERIOD), send_image)
    rospy.spin()
    print "node start"


if __name__ == '__main__':
    cam_listener()
