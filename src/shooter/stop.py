#!/usr/bin/env python
import rospy
from geometry_msgs.msg import Twist


class StopRobot:
    def __init__(self):
        rospy.init_node('stop', anonymous=True)
        self.__pub = rospy.Publisher('cmd_vel', Twist, queue_size=10)
        self.__scan_filter = []
        self.__twist = Twist()

    def stop(self):
        self.__check_and_pub_twist(0, 0)

    def __check_and_pub_twist(self, x, ang_z):
        self.__pub_twist(ang_z, x)

    def __pub_twist(self, ang_z, x):
        self.__twist.linear.x = x
        self.__twist.angular.z = ang_z
        self.__pub.publish(self.__twist)


if __name__ == "__main__":
    StopRobot().stop()
