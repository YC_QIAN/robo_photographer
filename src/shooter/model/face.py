# face in the camera, not in the kinetic
# left upper (0,0)
from rectangle import Rectangle


class Face:
    def __init__(self, left_upper_x, left_upper_y, width, height, depth):
        self.rect = Rectangle(left_upper_x, left_upper_y, width, height)
        self.depth = depth

