from enum import IntEnum


class FrameCondition(IntEnum):
    OUT_OF_BOUNDS_VERTICALLY = 1
    OUT_OF_BOUNDS_HORIZONTALLY = -1
    TOO_SMALL = 2
    NO_FRAME = 0
    FRAMED = -2
