from enum import IntEnum


class Direction(IntEnum):
    RIGHT = 1
    LEFT = -1
    STOP = 0
    FORWARD = 2

