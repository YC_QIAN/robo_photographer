class Rectangle:
    def __init__(self, x=0, y=0, width=0, height=0):
        self.x = x
        self.y = y
        self.width = width
        self.height = height

    def is_overlap(self, rc2):
        if self.x + self.width > rc2.x and\
                rc2.x + rc2.width > self.x and\
                self.y + self.height > rc2.y and\
                rc2.y + rc2.height > self.y:
            return True
        else:
            return False

    def get_overlap(self, rc2):
        if self.is_overlap(rc2):
            x11 = self.x
            x21 = rc2.x
            x12 = self.x + self.width
            x22 = rc2.x + rc2.width
            y11 = self.y
            y21 = rc2.y
            y12 = self.y + self.height
            y22 = rc2.y + rc2.height
            res = Rectangle(max(x11, x21), max(y11, y21),
                            min(x12, x22) - max(x11, x21),
                            min(y12, y22) - max(y11, y21))
            return res

    def get_covering_rec(self, rc2):
        res = Rectangle()
        res.x = min(self.x, rc2.x)
        res.y = min(self.y, rc2.y)
        x_max = max(self.x + self.width, rc2.x + rc2.width)
        y_max = max(self.y + self.height, rc2.y + rc2.height)
        res.width = x_max - res.x
        res.height = y_max - res.y
        return res

    def area(self):
        print "width "+str(self.width)
        print "height "+str(self.height)
        return self.width * self.height

    def __str__(self):
        return '(Rec: %s, %s, %s, %s)' % (self.x, self.y, self.width, self.height)


if __name__ == '__main__':
    a = Rectangle(1, 1, 3, 5)
    b = Rectangle(2, 3, 4, 1)
    print a.area()
    print a.is_overlap(b)
    print a.get_overlap(b)
    print a.get_covering_rec(b)