#!/usr/bin/env python
# GotoPoint
# refer to turtlebot3_teleop_key, turtlebot3_pointop_key to get more details

import rospy
from geometry_msgs.msg import Twist, Point
import numpy as np
import tf
from math import sqrt, pow, pi, atan2
from tf.transformations import euler_from_quaternion
from robo_photographer.srv import MoveRobot
from std_msgs.msg import Empty

# m/s
WAFFLE_MAX_LIN_VEL = 0.26
# rad/s
WAFFLE_MAX_ANG_VEL = 1.82

LIDAR_ERR = 0.05


# control your Turtlebot3!
# -----------------------
# Insert xyz - coordinate.
# x : position x (m) + -> left
# y : position y (m)
# z : orientation z (degree: -180 ~ 180) + -> left

class GotoPoint:
    def __init__(self):
        rospy.init_node("move_robot")
        s = rospy.Service('move_robot', MoveRobot, self.execute_cb)

        self.tf_listener = tf.TransformListener()

        # rospy.init_node('turtlebot3_pointop_key', anonymous=False)
        # rospy.on_shutdown(self.shutdown)
        self.cmd_vel = rospy.Publisher('cmd_vel', Twist, queue_size=5)
        position = Point()
        move_cmd = Twist()
        self.r = rospy.Rate(10)
        self.odom_frame = 'odom'

        try:
            self.tf_listener.waitForTransform(self.odom_frame, 'base_footprint', rospy.Time(), rospy.Duration(1.0))
            self.base_frame = 'base_footprint'
        except (tf.Exception, tf.ConnectivityException, tf.LookupException):
            try:
                self.tf_listener.waitForTransform(self.odom_frame, 'base_link', rospy.Time(), rospy.Duration(1.0))
                self.base_frame = 'base_link'
            except (tf.Exception, tf.ConnectivityException, tf.LookupException):
                rospy.loginfo("Cannot find transform between odom and base_link or base_footprint")
                rospy.signal_shutdown("tf Exception")

        print "service ready"
        rospy.spin()

        # self.__reset_odom = rospy.Publisher('/mobile_base/commands/reset_odometry', Empty, queue_size=10)

    def execute_cb(self, req):

        goal_x = float(req.x)
        goal_y = float(req.y)
        goal_z = float(req.theta)
        (position, rotation) = self.get_odom()
        print "initial" + str(position) + " " + str(rotation)

        # reset goal_x and goal_y according to the current position
        goal_x = goal_x + position.x
        goal_y = goal_y + position.y

        last_rotation = 0
        linear_speed = 1
        angular_speed = 1
        if goal_z > 180 or goal_z < -180:
            print("you input wrong z range.")
            self.shutdown()
        # goal_z = np.deg2rad(goal_z)
        goal_distance = sqrt(pow(goal_x - position.x, 2) + pow(goal_y - position.y, 2))
        distance = goal_distance
        move_cmd = Twist()

        # move
        while distance > 0.05:
            print "distance "+str(distance)
            (position, rotation) = self.get_odom()
            x_start = position.x
            y_start = position.y
            path_angle = atan2(goal_y - y_start, goal_x - x_start)

            if path_angle < -pi / 4 or path_angle > pi / 4:
                if goal_y < 0 and y_start < goal_y:
                    path_angle = -2 * pi + path_angle
                elif goal_y >= 0 and y_start > goal_y:
                    path_angle = 2 * pi + path_angle
            if last_rotation > pi - 0.1 and rotation <= 0:
                rotation = 2 * pi + rotation
            elif last_rotation < -pi + 0.1 and rotation > 0:
                rotation = -2 * pi + rotation
            move_cmd.angular.z = angular_speed * path_angle - rotation

            distance = sqrt(pow((goal_x - x_start), 2) + pow((goal_y - y_start), 2))
            move_cmd.linear.x = min(linear_speed * distance, 0.1)

            if move_cmd.angular.z > 0:
                move_cmd.angular.z = min(move_cmd.angular.z, 1.5)
            else:
                move_cmd.angular.z = max(move_cmd.angular.z, -1.5)

            last_rotation = rotation
            self.cmd_vel.publish(move_cmd)
            self.r.sleep()
        (position, rotation) = self.get_odom()

        # rotate
        while abs(rotation - goal_z) > 0.05:
            (position, rotation) = self.get_odom()
            if goal_z >= 0:
                if rotation <= goal_z and rotation >= goal_z - pi:
                    move_cmd.linear.x = 0.00
                    move_cmd.angular.z = 0.5
                else:
                    move_cmd.linear.x = 0.00
                    move_cmd.angular.z = -0.5
            else:
                if rotation <= goal_z + pi and rotation > goal_z:
                    move_cmd.linear.x = 0.00
                    move_cmd.angular.z = -0.5
                else:
                    move_cmd.linear.x = 0.00
                    move_cmd.angular.z = 0.5
            self.cmd_vel.publish(move_cmd)
            self.r.sleep()

        print abs(rotation - goal_z)
        rospy.loginfo("Stopping the robot...")
        self.cmd_vel.publish(Twist())
        # self.__reset_odom.publish(Empty())
        return True

    def get_odom(self):
        try:
            (trans, rot) = self.tf_listener.lookupTransform(self.odom_frame, self.base_frame, rospy.Time(0))
            rotation = euler_from_quaternion(rot)

        except (tf.Exception, tf.ConnectivityException, tf.LookupException):
            rospy.loginfo("TF Exception")
            return
        print Point(*trans), rotation[2]
        return Point(*trans), rotation[2]

    def shutdown(self):
        self.cmd_vel.publish(Twist())
        rospy.sleep(1)


if __name__ == "__main__":
    GotoPoint()
