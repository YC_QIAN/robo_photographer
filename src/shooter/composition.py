from model.direction import Direction
from model.rectangle import Rectangle
from model.frame_condition import FrameCondition
from face_detector.runner import
import numpy as np
import math
import time

# simplified luke
# including non kinect-camera transfer

MAX_TIME_FOR_FRAMING_S = 60
MAX_DEVIATION_FROM_CORRECT_FRAMING_PERCENT = 50

# todo set params
# CAMERA_FRAME_WIDTH = 4320
# CAMERA_FRAME_HEIGHT = 3240
CAMERA_FRAME_WIDTH = 1680
CAMERA_FRAME_HEIGHT = 1048

# MINIMAL_PICTURE_FRAME_WIDTH = 2160
# MINIMAL_PICTURE_FRAME_HEIGHT = 1620
MINIMAL_PICTURE_FRAME_WIDTH = 0
MINIMAL_PICTURE_FRAME_HEIGHT = 0

CAMERA_FRAME_RECTANGLE = Rectangle(0, 0, CAMERA_FRAME_WIDTH, CAMERA_FRAME_HEIGHT)


# @param faces: rectangle list, not face list!!!
# @param enable_time: the first time to start composition for this area, time.time()
# @return direction, frame_condition
def composition(faces, enable_time):
    # if no face in the picture
    if len(faces) == 0:
        return Direction.FORWARD, FrameCondition.NO_FRAME

    # Find the head rectangle closest to the center of the camera image plane
    closest_rectangle_index = _centermostHeadRectangleIndex(faces)

    # Try framing the person closest to the picture center
    middle_face = faces[closest_rectangle_index]
    frame = _frameSinglePerson(middle_face, True)

    # Check for additional candidates in the frame
    candidates = _getCandidatesInFrame(faces, frame)

    # Check whether framing a single person slightly off to the right yields better results
    if len(candidates) > 1:
        frame_right = _frameSinglePerson(middle_face, False)
        candidates_to_include_right = _getCandidatesInFrame(faces, frame_right)

        if len(candidates_to_include_right) < len(candidates):
            candidates = candidates_to_include_right
            frame = frame_right

    # If there are more people in our single-person frame, iteratively update the framing
    framed = (len(candidates) == 1)

    while not framed:
        # Find the bounding rectangle for all candidates
        bounding_rectangle = middle_face
        for candidate in candidates:
            bounding_rectangle = bounding_rectangle.get_covering_rec(candidate)

        # Check whether the bounding rectangle is "wide" or "narrow"
        if bounding_rectangle.width > 1.6 * bounding_rectangle.height:
            frame = _frameWideGroup(bounding_rectangle)
        else:
            frame = _frameNarrowGroup(bounding_rectangle)

        # Check whether the new frame contains any new candidates
        new_candidates = _getCandidatesInFrame(faces, frame)

        if candidates == new_candidates:
            framed = True
        else:
            candidates = new_candidates

    # Get the threshold for the framing quality based on the elapsed time
    # since the framing node has been enabled.
    elapsed_seconds_since_enable = time.time() - enable_time
    print "elapsed_seconds_since_enable "+str(elapsed_seconds_since_enable)

    current_max_deviation_from_correct_framing = \
        MAX_DEVIATION_FROM_CORRECT_FRAMING_PERCENT * elapsed_seconds_since_enable / MAX_TIME_FOR_FRAMING_S
    current_max_deviation_from_correct_framing = min(MAX_DEVIATION_FROM_CORRECT_FRAMING_PERCENT,
                                                     current_max_deviation_from_correct_framing)

    # Calculate the minimum and the current framing overlap percentage
    min_allowed_framing_overlap = 100.0 - current_max_deviation_from_correct_framing
    current_framing_overlap = 100.0 * (frame.get_overlap(CAMERA_FRAME_RECTANGLE)).area() / frame.area()
    print frame
    print frame.get_overlap(CAMERA_FRAME_RECTANGLE)
    #ROS_INFO("Current overlap: %f, allowed overlap: %f", current_framing_overlap, min_allowed_framing_overlap);

    # Clip the frame to the picture rectangle and ensure that the frame is at least of the minimal size
    frame = frame.get_overlap(CAMERA_FRAME_RECTANGLE)
    sufficient_frame_size = (frame.width >= MINIMAL_PICTURE_FRAME_WIDTH) and \
                            (frame.height >= MINIMAL_PICTURE_FRAME_HEIGHT)

    print "sufficient_frame_size " + str(sufficient_frame_size)
    print "current_framing_overlap "+str(current_framing_overlap)
    print "min_allowed_framing_overlap " +str(min_allowed_framing_overlap)
    print frame

    if sufficient_frame_size and current_framing_overlap > min_allowed_framing_overlap:
        framing_status = FrameCondition.FRAMED
        driving_direction = Direction.STOP
    else:
        if not sufficient_frame_size:
            framing_status = FrameCondition.TOO_SMALL
            driving_direction = Direction.FORWARD
        else:
            # Enclosing frame is out of the image plane. Try to center the composition.
            if (frame.y < 0) or frame.y + frame.height > CAMERA_FRAME_HEIGHT:
                framing_status = FrameCondition.OUT_OF_BOUNDS_VERTICALLY

            else: # Enclosing frame is out of the image plane width-wise

                framing_status = FrameCondition.OUT_OF_BOUNDS_HORIZONTALLY

            frame_center_x = frame.x + (frame.width / 2)
            driving_direction = Direction.RIGHT if (frame_center_x > (CAMERA_FRAME_WIDTH / 2)) else Direction.LEFT
            #ROS_INFO("Framing turn: %s", (driving_direction == RIGHT) ? "RIGHT" : "LEFT");
    return driving_direction, framing_status


def _centermostHeadRectangleIndex(faces):
    centermost_rectangle_index = 0
    min_distance_to_center = max(CAMERA_FRAME_WIDTH, CAMERA_FRAME_HEIGHT)
    frame_center = np.array([CAMERA_FRAME_WIDTH / 2, CAMERA_FRAME_HEIGHT / 2])
    for face in faces:
        head_rectangle_center = np.array([face.x + (face.width / 2),
                                          face.y + (face.height / 2)])
        dis = head_rectangle_center - frame_center
        current_distance_to_center = math.hypot(dis[0], dis[1])

        if current_distance_to_center < min_distance_to_center:
            min_distance_to_center = current_distance_to_center
            centermost_rectangle_index = faces.index(face)

    return centermost_rectangle_index


def _frameSinglePerson(face, off_center_left):
    s = 1.5 * face.height

    head_center_x = face.x + 0.5 * face.width
    head_center_y = face.y + 0.5 * face.height

    frame = Rectangle()
    frame.x = head_center_x - 2.0 * s
    frame.y = head_center_y - 1.2 * s

    frame.width = 4.0 * s
    frame.height = 3.0 * s

    # Put the face slightly off-center
    # todo: unimportant magic number
    horizontal_offset = (0.2 if off_center_left else -0.2) * face.width
    frame.x += horizontal_offset
    return frame


def _frameWideGroup(face):
    s = 0.375 * face.width

    bounding_rectangle_center_x = face.x + 0.5 * face.width
    bounding_rectangle_center_y = face.y + 0.5 * face.height

    frame = Rectangle()
    frame.x = bounding_rectangle_center_x - 2.0 * s
    frame.y = bounding_rectangle_center_y - 1.2 * s

    frame.width = 4.0 * s
    frame.height = 3.0 * s

    return frame


def _frameNarrowGroup(face):

    s = 0.5 * face.height

    bounding_rectangle_center_x = face.x + 0.5 * face.width
    bounding_rectangle_center_y = face.y + 0.5 * face.height

    frame = Rectangle()
    frame.x = bounding_rectangle_center_x - 2.0 * s
    frame.y = bounding_rectangle_center_y - 1.2 * s

    frame.width = 4.0 * s
    frame.height = 3.0 * s

    return frame


def _getCandidatesInFrame(faces, frame):
    candidates_in_frame = []
    for face in faces:
        if frame.is_overlap(face):
            candidates_in_frame.append(face)
    return candidates_in_frame


if __name__ == '__main__':
    print composition([Rectangle(0, 235, 200, 200)], time.time() - 55)



