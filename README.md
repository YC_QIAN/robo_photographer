# 基于百度人脸识别api的拍照机器人

## prerequisite
ROS kinetic installed

for convenience, check if the following lines exist in ~/.bashrc

```
source /opt/ros/kinetic/setup.bash
source ~/catkin_ws/devel/setup.bash

export ROS_HOSTNAME=xxx
export ROS_MASTER_URI=http://${ROS_HOSTNAME}:11311

alias cw='cd ~/catkin_ws'
alias cs='cd ~/catkin_ws/src'
alias cm='cd ~/catkin_ws && catkin_make'

export TURTLEBOT3_MODEL=waffle_pi
export PATH=/home/andi/python/bin:$PATH
```
`turtlebot3`, `turtlebot3_msgs`, `turtlebot3_simulations`
in `~/catkin/src`

check ROS_MASTER_URI of turtlebot
## download  
```
cs
git clone https://yourname@bitbucket.org/YC_QIAN/robo_photographer.git
```

Required by catkin, the ROS dedicated build system, path of the repo should be`~/catkin_ws/src/robo_photographer`, so make sure you clone the repo at the right place

## 依赖安装
python2.7+ or python3.6+
pip install baidu-aip

## 配置
在face.py中添加自定义的json encoder类，处理bytes数据

class MyEncoder(json.JSONEncoder):
```
    def default(self, o):
        if isinstance(o,bytes):
            return str(o,encoding='utf-8')
        else:
            return json.JSONEncoder.default(self,o)
```

同时，修改detect方法，在json.dumps()方法的调用中添加cls=MyEncoder

## bring up
[Remote PC]

`roscore`

[Turtlebot]
```
roslaunch turtlebot3_bringup turtlebot3_robot.launch
```

## how to run a node 

(take raspicam_node for example)

[Turtlebot]

`roslaunch raspicam_node camerav2_1280x960.launch`



if you want to view the published image

[Remote PC]

`rqt_image_view`



[Remote PC]

```
cw
roslaunch robo_photographer cam_listener.launch
```





